# DevContainer Sandbox

DevContainer testing

## Requirements

* vscode
* [vscode Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)
* docker
* A working ssh-agent to setup and use the container
* [The ssh configuration](ssh_config) up and running

## Intent

Try to create a container working both as devcontainer and ssh remote

## Modus Operandi

1. Create the _ssh container_ with `run_ssh_container`
    * The container will have you local pubkeys from your ssh agent configured on the hugo user
2. Try to connect with `ssh hugo`
3. Try to connect with the _SSH Remote_ command from vscode
4. Use _clone.sh_ command in the container to get the code _in_ the container
5. Open the code base and work on it

## What is not working

I managed to have everything working until the git clone command. But it is not working anymore. Don't know why.
Further aspects have not been tested.
