#!/bin/bash

# Start SSH server
/usr/sbin/sshd -D

# To keep the container running
tail -f /dev/null
