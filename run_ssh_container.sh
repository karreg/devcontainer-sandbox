#!/bin/bash

cp ~/.gitconfig rgitconfig
ssh-add -L > authorized_keys
GIT_URL=$(git remote get-url origin)
echo "git clone ${GIT_URL} workspace" > clone.sh && chmod +x clone.sh

docker kill stories
docker rm stories
docker build -f .devcontainer/Dockerfile -t stories:latest .

rm -f rgitconfig clone.sh authorized_keys

docker run --name stories -d -p 8022:22 stories:latest